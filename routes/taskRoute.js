// Defines WHEN particular controllers will be used
// Contain all the endpoints and responses that we can get from controllers

const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();
const taskController = require("../controllers/taskController")


// Route to get all the tasks
// http://location:3001/tasks
router.get("/:id" , (req , res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});


//Route to create task
router.put("/",(req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});




module.exports = router;


