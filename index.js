
const express = require ("express");
const mongoose = require ("mongoose");


const taskRoute = require("./routes/taskRoute");




const app = express();
const port = 3001;


app.use(express.json());
app.use(express.urlencoded({extended : true}));

mongoose.connect("mongodb+srv://mjbarnes03:admin123@cluster0.kasjqle.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

	}
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));

app.use("/tasks", taskRoute)

app.listen(port, () => console.log(`Now listening to port ${port}.`));