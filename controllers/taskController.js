// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

const Task = require("../models/task");

// COntroller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
};


//Controller function for creating a task
module.exports.createTask = (requestBody) => {

	let newTask = new Task ({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};



